#include <avr/sleep.h>

// Setting pins of colors
int yellow = 3;
int white = 5;
int orange = 6;

// Enable LED's
int enLed = 4;

// Port to enable level sensor
int enSensor = 7;

// Port to light sensor
int lightSensor = 2;

// Setting interval between pwm changes
int interval = 25;

int outArray[3][5] = {
  {yellow,0,0,100,0}, //port, step, fraction, intensity
  {white,0,0,60,0},
  {orange,0,0,100,0}
};
int steps[3][10] = {
  {0,1,2,3,4,5,6,10,12,14},  //Steps for starting
  {14,12,10,6,5,5,5,1,1,0}   //Steps for fading
};
int stepsLength = 10;

int levelCount = 0;
int wrongLevelCount = 0;
int level = 0;
int variation = 0;
int lowVoltage = false;
bool isNight = false;

unsigned long dayRunTime = 15;
unsigned long nightRunTime = 180;
unsigned long previousMillis = 0;

// Run steps from array (for soft start/fading)
void runStep(int port, int _type) {
  int output = getConfig(port);
  float _intensity = outArray[output][3];
  
  int _step = outArray[output][1];
  int _next = _step+1;

  int _fraction = outArray[output][2];
  int _brightness = steps[_type][_step]*16;
  
  if (_step >= stepsLength - 1)
    _next = 0;

  // Increasing/decreasing output intensity in small steps for smoothness
  if (steps[output][_next] >= steps[output][_step]){
    _brightness += (steps[_type][_next] - steps[_type][_step])*_fraction;
    _fraction++;
  }
  else {
    _brightness -= (steps[_type][_step] - steps[_type][_next])*_fraction;
    _fraction++;
  }

  if (_fraction >= 16) {
    _fraction = 0;
    _step++;
  }

  if (_step >= stepsLength)
    _step = 0;

  // Setting intensity for a specific color
  if (_intensity < 100)
      _brightness = _brightness*(_intensity/100);

  // Adding some noise to the output
  if (_type == 0)
    _brightness = _brightness - random(0, _brightness/4);
  else
    _brightness = _brightness - random(0, _brightness);

  analogWrite(port, _brightness ^ 0xff);
  outArray[output][4] = _brightness;
  
  outArray[output][1] = _step;
  outArray[output][2] = _fraction;
}

// Normal cycle with random noise (for default operation)
void randomCycle(int port) {
  int output = getConfig(port);
  float _intensity = outArray[output][3];

  int _var = variation*5 + level;
  _var = constrain(_var, 0, 210);
  
  int _min = abs(210 - _var);
  int _max = abs(255 - level);

  // Adding noise to output
  int _brightness = random(_min,_max);

  // Setting color's intensity
  if (_intensity < 100)
      _brightness = _brightness*(_intensity/100);
  
  analogWrite(port, _brightness ^ 0xff);
  outArray[output][4] = _brightness;
}

// Reading level sensor
void getLevel() {
  levelCount++;
  int _level = 0;

  // Avoid reading sensor every time
  if (levelCount == 300/interval) {

    // Turn on sensor for ADC reading
    digitalWrite(enSensor, HIGH);
    _level = analogRead(A0);
    digitalWrite(enSensor, LOW);

    _level = map(_level, 0, 700, 0, 255);
    variation = abs(_level - level);
    
    variation = map(variation, 0, 700, 0, 255);
    level = _level;
    levelCount = 0;
  }
}

// Run starting/fading cycles
void stepsCycle(int mode) {
  if (mode == 1)
    setFadingStep();
  else {    
    for (int i=0; i<3; i++) {
      outArray[i][1] = 0;
      outArray[i][2] = 0;
      outArray[i][4] = 0;
    }
  }

  level = 0;
  while(outArray[2][1] < stepsLength-1) { 
    runStep(yellow, mode);
    runStep(white, mode);
    runStep(orange, mode);
    delay(50);

    getLevel();
    if (mode != 1 && level > 60)
      break;
  }
}

// Set current brightness status before fading
void setFadingStep() {
  for (int i=0; i<stepsLength; i++)
    if (steps[1][i] *16 <= outArray[0][4]) {
      for (int j=0; j<3; j++)
        outArray[j][1] = i;

      break;
    }
}

void checkStatus() {
  if (level > 180)
    wrongLevelCount++;
  else
    wrongLevelCount = 0;

  // Check if level is wrong for too long
  if(wrongLevelCount > 100) {
    wrongLevelCount = 0;

    goSleep();
  }
}

// Check if battery voltage is too low (140 ~ 3.1V)
void checkVoltage() {
  if (analogRead(A2) >= 140) {
    delay(100);
    if (analogRead(A2) >= 140) {
      lowVoltage = true;
      goSleep();
    }
  }
}

void wakeUp() {
}

void goSleep() {

  // Run fading effect
  if (!lowVoltage)
    stepsCycle(1);

  digitalWrite(enLed, LOW);
  digitalWrite(yellow, HIGH);
  digitalWrite(white, HIGH);
  digitalWrite(orange, HIGH);


  // Disable ADC
  ADCSRA = 0;

  // Set sleep mode
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();

  // Set interrupt pin for wakeup
  if (!lowVoltage)
    attachInterrupt(0, wakeUp, FALLING);
  
  sleep_mode();

  // Disabling sleep, enabling ADC and detaching interrupt after wakeup
  sleep_disable();
  ADCSRA |= (1 << ADEN);
  detachInterrupt(0);

  // Run wakeup effect
  digitalWrite(enLed, HIGH);
  stepsCycle(0);
}

int getConfig(int color) {
  for (int i=0; i<3; i++)
    if (outArray[i][0] == color)
      return i;
}

// Check if it's time to turn off the lights
void checkTime() {
  unsigned long timeLimit = 0;
  unsigned long currentMillis = millis();
  
  bool _lightSensor = !digitalRead(lightSensor);

  if(isNight)
    timeLimit = 60000 * nightRunTime;
  else
    timeLimit = 60000 * dayRunTime;

  if(_lightSensor != isNight) {
    isNight = _lightSensor;
    previousMillis = currentMillis;
  }

  if ((currentMillis - previousMillis) >= timeLimit) {
    previousMillis = currentMillis;
    goSleep();
  }
}

void setup() {
  checkVoltage();
  
  pinMode(enLed, OUTPUT);
  pinMode(enSensor, OUTPUT);
  pinMode(yellow, OUTPUT);
  pinMode(white, OUTPUT);
  pinMode(orange, OUTPUT);

  digitalWrite(yellow, HIGH);
  digitalWrite(white, HIGH);
  digitalWrite(orange, HIGH);
  digitalWrite(enLed, HIGH);

  pinMode(lightSensor, INPUT);

  // Disable level sensor
  digitalWrite(enSensor, LOW);

  // Start random function with a random number
  randomSeed(analogRead(A1));

  // Serial.begin(9600);
  stepsCycle(0);
}

void loop() {
  checkVoltage();
  
  randomCycle(yellow);
  randomCycle(white);
  randomCycle(orange);

  getLevel();
  checkStatus();
  checkTime();

  delay(interval);
}
