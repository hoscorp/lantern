#include <avr/sleep.h>
#include <avr/wdt.h>

// Setting pins of colors
const int yellow = 0;
const int white = 1;

// Level sensor's pin (Analog and digital for setting pull up resistor, pin 7)
const int levelSensor = A1;
const int enLevelSensor = 2;

// LDR pin (pin 2)
const int lightSensor = A3;
const int enLightSensor = 3;

// Diode voltage sensor pin (pin 3)
const int powerSensor = A2;
const int enPowerSensor = 4;

// Setting interval between pwm changes
const int interval = 25;

// Setting day and night runtime in minutes
const int dayRunTime = 15;     // 15
const int nightRunTime = 300; // 300

// Setting power on/off limits
const int lowPwrOff = 173;
const int lowPwrOn = 148;

// Setting level limit
const int levelLimit = 100;

// Setting light limit
const int lightLimit = 175;

int outArray[2][5] = {
  { yellow, 0, 0, 100, 0 },  //port, step, fraction, intensity
  { white, 0, 0, 100, 0 }
};

int steps[2][10] = {
  { 0, 1, 2, 3, 4, 5, 6, 10, 12, 14 },  //Steps for starting
  { 14, 12, 10, 6, 5, 5, 5, 1, 1, 0 }   //Steps for fading
};
int stepsLength = 10;

int levelCount = 0;
int wrongLevelCount = 0;
int level = 0;
int variation = 0;
bool isNight = false;
bool lightStatus = false;
bool lowPower = false;

unsigned long previousMillis = 0;


// Run steps from array (for soft start/fading)
void runStep(int port, int _type) {
  int output = getConfig(port);
  float _intensity = outArray[output][3];

  int _step = outArray[output][1];
  int _next = _step + 1;

  int _fraction = outArray[output][2];
  int _brightness = steps[_type][_step] * 16;

  if (_step >= stepsLength - 1)
    _next = 0;

  // Increasing/decreasing output intensity in small steps for smoothness
  if (steps[output][_next] >= steps[output][_step]) {
    _brightness += (steps[_type][_next] - steps[_type][_step]) * _fraction;
    _fraction++;
  } else {
    _brightness -= (steps[_type][_step] - steps[_type][_next]) * _fraction;
    _fraction++;
  }

  if (_fraction >= 16) {
    _fraction = 0;
    _step++;
  }

  if (_step >= stepsLength)
    _step = 0;

  // Setting intensity for a specific color
  if (_intensity < 100)
    _brightness = _brightness * (_intensity / 100);

  // Adding some noise to the output
  if (_type == 0)
    _brightness = _brightness - random(0, _brightness / 4);
  else
    _brightness = _brightness - random(0, _brightness);

  // analogWrite(port, _brightness ^ 0xff);
  analogWrite(port, _brightness);
  outArray[output][4] = _brightness;

  outArray[output][1] = _step;
  outArray[output][2] = _fraction;
}

// Normal cycle with random noise (for default operation)
void randomCycle(int port) {
  int output = getConfig(port);
  float _intensity = outArray[output][3];

  int _var = variation * 5 + level;
  _var = constrain(_var, 0, 210);

  int _min = abs(210 - _var);
  int _max = abs(255 - level);

  // Adding noise to output
  int _brightness = random(_min, _max);

  // Setting color's intensity
  if (_intensity < 100)
    _brightness = _brightness * (_intensity / 100);

  // analogWrite(port, _brightness ^ 0xff);
  analogWrite(port, _brightness);
  outArray[output][4] = _brightness;
}

// Reading level sensor
void getLevel() {
  levelCount++;
  int _level = 0;

  // Avoid reading sensor every time
  if (levelCount == 300 / interval) {

    // Turn on sensor for ADC reading
    pinMode(enLevelSensor, INPUT_PULLUP);
    _level = analogRead(levelSensor);
    pinMode(enLevelSensor, INPUT);   

    _level = _level ^ 0b1111111111;
    _level = map(_level, 0, 700, 0, 255);
    
    variation = abs(_level - level);

    variation = map(variation, 0, 700, 0, 255);
    level = _level;
    levelCount = 0;
  }
}

// Run starting/fading cycles
void stepsCycle(int mode) {
  if (mode == 1)
    setFadingStep();
  else {
    for (int i = 0; i < 2; i++) {
      outArray[i][1] = 0;
      outArray[i][2] = 0;
      outArray[i][4] = 0;
    }
  }

  level = 0;
  while (outArray[1][1] < stepsLength - 1) {
    runStep(yellow, mode);
    runStep(white, mode);
    delay(50);

    getLevel();
    if (mode != 1 && level > 60)
      break;
  }
}

// Set current brightness status before fading
void setFadingStep() {
  for (int i = 0; i < stepsLength; i++)
    if (steps[1][i] * 16 <= outArray[0][4]) {
      for (int j = 0; j < 2; j++)
        outArray[j][1] = i;

      break;
    }
}

void checkStatus() {
  if (level > levelLimit)
    wrongLevelCount++;
  else
    wrongLevelCount = 0;

  // Check if level is wrong for too long
  if (wrongLevelCount > 100) {
    wrongLevelCount = 0;

    goSleep();
  }
}

void resetWatchdog()
{
  WDTCR |= (1<<WDP3) | (0<<WDP2) | (0<<WDP1) | (0<<WDP1);
  WDTCR |= (1<<WDIE);
  WDTCR |= (0<<WDE);

  wdt_reset();  
}

bool checkPower() {
  pinMode(enPowerSensor, INPUT_PULLUP);
  int sensor = analogRead(powerSensor);
  pinMode(enPowerSensor, INPUT);

  if (lowPower) {
    if (sensor >= lowPwrOn)
      return false;
    else
      lowPower = false;
  }
  else if (sensor >= lowPwrOff) {
    lowPower = true;
    return false;
  }

  return true;
}

// Watchdog Interrupt Service / is executed when watchdog timed out
ISR(WDT_vect) {
  wdt_disable();
}

void goSleep() {

  // Run fading effect
  stepsCycle(1);

  digitalWrite(yellow, LOW);
  digitalWrite(white, LOW);

  sleep:

  // turn off ADC
  ADCSRA = 0;    
  
  // enable Interrupts         
  sei();

  // get watchdog ready
  resetWatchdog(); 

  // Set sleep mode
  set_sleep_mode (SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  sleep_mode();
  
  // for precaution
  sleep_disable();      

  // turn on ADC
  ADCSRA |= (1 << ADEN);

  // Go back to sleep if nothing has changed
  bool lastLightStatus = lightStatus;
  
  if (!checkPower() || checkLight() == lastLightStatus || lightStatus)
    goto sleep;

  // Reset light timer
  previousMillis = millis();

  // Start random function with a random number
  randomSeed(analogRead(levelSensor));  
  
  // Run wakeup effect
  stepsCycle(0);
}

int getConfig(int color) {
  for (int i = 0; i < 2; i++)
    if (outArray[i][0] == color)
      return i;
}

bool checkLight() {
  pinMode(enLightSensor, INPUT_PULLUP);
  int sensor = analogRead(lightSensor);
  pinMode(enLightSensor, INPUT);

  if (sensor < lightLimit) {
    lightStatus = true;
    return true;
  }
  else {
    lightStatus = false;    
    return false;
  }
}

// Check if it's time to turn off the lights
void checkTime() {
  unsigned long timeLimit = 0;
  unsigned long currentMillis = millis();

  if (isNight)
    timeLimit = 60000 * nightRunTime;
  else
    timeLimit = 60000 * dayRunTime;

  bool _lightSensor = !checkLight();

  if (_lightSensor != isNight) {
    for (int i = 0; i < 10; i++) {
      _lightSensor = !checkLight();
      
      if (_lightSensor == isNight)
        break;

      delay(5);
    }
  }  

  if (_lightSensor != isNight) {
    isNight = _lightSensor;
    previousMillis = currentMillis;
  }

  if ((currentMillis - previousMillis) >= timeLimit) {
    previousMillis = currentMillis;
    goSleep();
  }
}

void setup() {
  wdt_disable();

  pinMode(yellow, OUTPUT);
  pinMode(white, OUTPUT);

  digitalWrite(yellow, LOW);
  digitalWrite(white, LOW);

  // Start random function with a random number
  randomSeed(analogRead(levelSensor));

  stepsCycle(0);
}

void loop() {
  randomCycle(yellow);
  randomCycle(white);

  getLevel();
  checkStatus();
  checkTime();
  
  if (!checkPower())
    goSleep();
      
  delay(interval);
}
